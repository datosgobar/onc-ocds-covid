# ONC OCDS Covid

Oficina Nacional de Contrataciones - Estándar de Datos Open Contracting para las Contrataciones Abiertas

La presente guía incluye documentación de la publicación de los datos de Compras y Contrataciones del Gobierno de la República Argentina bajo el estándar Open Contracting en el marco de las contrataciones de emergencia por la pandemia Covid.

El Estándar de Datos para las Contrataciones Abiertas (OCDS) es un estándar global de datos que promueve la divulgación de datos y documentos en todas las etapas del proceso de contratación. Este estándar ha sido desarrollado para que los proveedores de información compartan datos estructurados, estandarizados y reutilizables.

La publicación de los datos de contratación se encuentran disponibles en datos.argentina.gob.ar (en este dataset) y tiene como objetivo garantizar que toda la información que se produce en las compras y contrataciones públicas de bienes y servicios en la ciudad estén disponibles en línea y en formatos abiertos.

Cualquier duda o consulta podés escribirnos a mail@jefatura.gob.ar.

